<html>
  <head>
    <meta charset="UTF-8">
    <!-- CSS -->
    <link rel="stylesheet" href="./css/style.css">

    <!-- JavaScript -->
    <script type="text/javascript" src="./javascript/widget.js"></script>
    <script type="text/javascript" src="./javascript/display.js"></script>

    <!-- JSon file containing some tweets -->
    <script type="text/json" src="../back/data/tweets.json"></script>
  </head>
  <body>

    <!-- MAIN CONTAINER -->
    <div id="mainContainer">
      <!-- Sub containers -->

    </div>
  </body>
</html>