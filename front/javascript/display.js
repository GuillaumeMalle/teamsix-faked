/* TWEETS CONTAINERS */
// File containing the tweets
var jSonTweetFilePath = "../back/data/tweets.json";
// File containing the generated tweets
var jSonFakeFilePath = "../back/data/fakes.json";

// Array containing the fake tweets (json parsed)
var jSonFakeList = [];
// Array containing the tweets (json parsed)
var jSonTweetList = [];
// Final list containing the tweets and fake tweets for sequence
var tweetList = [];
// Amount of fake tweets in the list
var noFakeTweets;


/* SEQUENCE */
// A sequence should start/is running or not
var sequence = false;
// The amount of tweet being displayed for each sequence
var tweetsPerSequence = 20;


/* DISPLAY */
// Increasing id of the blockquotes
var index = 0;
// Contains all the 
var listQuote = [];
// Size/resolution of the available screen
var screen = {
  x: window.innerWidth,
  y: window.innerHeight
};
// Position of the last quote displayed
var lastPos = {
  x: 0,
  y: 0
};


/* AUDIO */
var audioFile = "pop_notif.wav";



/************************************************************
** MAIN
************************************************************/


// On document ready
window.addEventListener("DOMContentLoaded", (event) => {
  InitStart();
});


// Called every 4 seconds
window.setInterval(function() {
  if (sequence) {
    RunSequence();
  }
}, 4000);


/*
** Return the mainContainer
*/
function GetMainContainer () {
  return document.getElementById("mainContainer");
}



/************************************************************
** START SCREEN
************************************************************/


/*
** Intialize the start screen
** Called on document ready
*/
function InitStart () {
  var mainContainer = document.getElementById("mainContainer");

  var startContainer = document.createElement("div");
  startContainer.id = "startContainer";

  var startLogo = document.createElement("img");
  startLogo.id = "startLogo";
  startLogo.classList.add("DefaultFontStyle", "MainMessage");
  startLogo.src = "./assets/twitter_logo.png";
  startContainer.appendChild(startLogo);

  var startButton = document.createElement("button");
  startButton.id = "startButton";
  startButton.classList.add("DefaultFontStyle", "MainMessage");
  startButton.innerHTML = "Start";
  startButton.addEventListener("click", InitSequence);
  startContainer.appendChild(startButton);

  mainContainer.appendChild(startContainer);
}


/*
** Remove the start container
*/
function CleanStartScreen (mainContainer) {
  var startContainer = document.getElementById("startContainer");
  mainContainer.removeChild(startContainer);
}



/************************************************************
** START SEQUENCE
************************************************************/


/*
** Initialize the sequence screen
*/
function InitSequence () {
  var mainContainer = GetMainContainer();

  CleanStartScreen (mainContainer);

  // Create container for the sequence
  var sequenceContainer = document.createElement("div");
  sequenceContainer.id = "sequenceContainer";
  mainContainer.appendChild(sequenceContainer);

  ChangeBackgroundColor("sequence");
  ShowStartMessage(sequenceContainer);
  StartSequence();
}


/*
** Create instructions
*/
function ShowStartMessage (sequenceContainer) {
  var startMsg = document.createElement("span");
  startMsg.id = "startMsg";
  startMsg.classList.add("DefaultFontStyle", "MainMessage");
  startMsg.innerHTML = "We have generated fake tweets, some may be in this sequence.";

  var startMsg2 = document.createElement("span");
  startMsg2.id = "startMsg";
  startMsg2.classList.add("DefaultFontStyle", "MainMessage");
  startMsg2.innerHTML = "Try to find them !";

  sequenceContainer.appendChild(startMsg);
  sequenceContainer.appendChild(startMsg2);
}


/*
** Change background color for sequence or start screen
*/
function ChangeBackgroundColor (step) {
  if (step == "start") {
    document.body.style.background = "rgb(58, 136, 254)";
  } else if (step == "sequence") {
    document.body.style.background = "rgb(21,32,43)";
  }
}


/*
** Store the tweets from json file into a list and start the sequence
*/
function StartSequence () {
  // Parse Json file
  loadTweetJSON(function(tweetResponse) {

    loadFakeJSON(function(fakeResponse) {
      // Store the fakes from json file in a list
      jSonFakeList = JSON.parse(fakeResponse).tweets;
      // Store the tweets from json file in a list
      jSonTweetList = JSON.parse(tweetResponse).tweets;
      // Generate a new list composed of "real" and fake tweets
      GenerateSequenceTweetList();

      // Reset sequence index
      index = 0;

      // Define the sequence state as true
      sequence = true;
    });
  });
}


/*
** Read the json file containing the real tweets
*/
function loadTweetJSON (callback) {
  var xobj = new XMLHttpRequest();

  xobj.overrideMimeType("application/json");

  xobj.open('GET', jSonTweetFilePath, true);

  xobj.onreadystatechange = function () {
    if (xobj.readyState == 4 && xobj.status == "200") {
      callback(xobj.responseText);
    }
  };
  xobj.send(null);
}


/*
** Read the json file containing the generated tweets
*/
function loadFakeJSON (callback) {
  var xobj = new XMLHttpRequest();

  xobj.overrideMimeType("application/json");

  xobj.open('GET', jSonFakeFilePath, true);

  xobj.onreadystatechange = function () {
    if (xobj.readyState == 4 && xobj.status == "200") {
      callback(xobj.responseText);
    }
  };
  xobj.send(null);
}


/*
** Generate a list containing real and fake tweets
*/
function GenerateSequenceTweetList () {
  // Get a random amount of fakes
  var noFakeTweets = randomIntBetween(0, jSonFakeList.length);
  // Amount of fakes already added to the list
  var currentFakeTweets = 0;

  for (var i = 0; i < tweetsPerSequence; i++) {
    var real  = true;
    // Get fake tweets only while  amount of fakes hasn't been reached
    if (currentFakeTweets < noFakeTweets) {
      // If the next will be a real tweet or not
      real = Math.random() >= 0.3;
    }

    if (real) {
      // Get a random index in the jSon list
      var rndIndex = randomIntBetween(0, jSonTweetList.length -1);

      // Add element at rndIndex from the jSon list to the new array
      tweetList.push(jSonTweetList[rndIndex]);
      // Remove the element at index rndIndex in the list to avoid duplicates
      jSonTweetList.splice(rndIndex, 1);
    }
    else
    {
      // Get a random index in the jSon list
      var rndIndex = randomIntBetween(0, jSonFakeList.length - 1);

      // Add element at rndIndex from the jSon list to the new array
      tweetList.push(jSonFakeList[rndIndex]);
      // Remove the element at index rndIndex in the list to avoid duplicates
      jSonFakeList.splice(rndIndex, 1);
      currentFakeTweets += 1;
    }
  }
}



/************************************************************
** SEQUENCE
************************************************************/


/*
** Main function called each iteration
*/
function RunSequence () {
  // End the sequence and clean the screen if enough tweets have been displayed
  CheckSequenceEnd();

  // Remove oldest tweets
  RemoveOldTweet();

  // Display a new tweet for this iteration
  DisplayTweet(tweetList[index]);
  //setTimeout(PlaySound, 2500, audioFile);

  // Increase the index
  IncreaseIndex();
}


/*
** End the sequence and clean the screen if enough tweets have been displayed
*/
function CheckSequenceEnd () {
  // Index reached the amount of tweets per sequence
  if (index == tweetsPerSequence)
  {
    sequence = false;
    setTimeout(InitForm, 15000);
    setTimeout(CleanSequenceScreen, 30000);
  }
}


/*
** Shift the oldest tweet (first in the list)
*/
function RemoveOldTweet () {
  if (index >= 7 && listQuote[0])
  {
    document.getElementById(listQuote[0].id).remove();
    listQuote.shift();
  }
}


/*
** Simply increase the index by one
*/
function IncreaseIndex () {
  index += 1;
}


/*
** Remove all elements from the sequence screen
*/
function CleanSequenceScreen () {
  var mainContainer = GetMainContainer();
  var sequenceContainer = document.getElementById("sequenceContainer");

  mainContainer.removeChild(sequenceContainer);
}


/************************************************************
** TWEET DISPLAY
************************************************************/


/*
** Display a tweet with the given tweet information
*/
function DisplayTweet (tweetObj) {
  var sequenceContainer = document.getElementById("sequenceContainer");

  // get screen size to adapt the tweet display
  GetScreenSize();

  // The new generated tweet container (blockquote)
  var tweetContainer = GenerateTweetContainer(tweetObj.tweetId, tweetObj.account);
  sequenceContainer.appendChild(tweetContainer);

  // Set a position for the new generated tweet
  SetTweetPosition(tweetContainer);

  // Add the tweet to the list
  listQuote.push(tweetContainer);

  twttr.widgets.load(tweetContainer);
}


/*
** Update screen size
*/
function GetScreenSize () {
  screen.x = window.innerWidth;
  screen.y = window.innerHeight;
}


/*
** Create a container with a Twitter embbed blockquote
*/
function GenerateTweetContainer (id, screenName) {
  // Generate the url from the given id and screenName
  var url = "https://twitter.com/" + screenName + "/status/" + id;

  // This div contains all the tweet containers
  var content = document.getElementById("content");

  // Tweet container
  var tweetContainer = document.createElement("div");
  tweetContainer.id = "tweetContainer_" + index;

  // Blockquote
  var bq = document.createElement("blockquote");
  bq.classList.add("twitter-tweet");
  tweetContainer.appendChild(bq);

  // Link
  var a = document.createElement("a");
  a.setAttribute('href', url);
  bq.appendChild(a);

  return tweetContainer;
}


/*
** Set a random position for the given tweet depending on his size
*/
function SetTweetPosition (tweetContainer) {
  var tweetWidth = Math.min(500, tweetContainer.offsetWidth);

  var pos = GetRandomPosition(tweetWidth);

  tweetContainer.style.width = tweetWidth;
  tweetContainer.style.left = pos.x;
  tweetContainer.style.top = pos.y;
  tweetContainer.style.position = "fixed";
}


/*
** Return a random position in available space
*/
function GetRandomPosition(tweetWidth) {
  available = false;
  // While the random position isn't available
  while (!available)
  {
    // Define random position
    var pos = {
      x : randomIntBetween(0, screen.x - tweetWidth),
      y : randomIntBetween(0, screen.y - tweetWidth)
    };

    // If the new position is on the same side as the last one
    if (!(pos.x <= screen.x / 2 && lastPos.x <= screen.x / 2)
      && !(pos.x >= screen.x / 2 && lastPos.x >= screen.x / 2))
    {
      available = true;
    }
  }

  // Save the new position
  lastPos = pos;

  return pos;
}


/*
** Get random int between two values
*/
function randomIntBetween(min, max) {
  return Math.floor(Math.random() * (max - min + 1) + min);
}


/*
** Play the sound from the given file
*/
function PlaySound(audioFile) {
  var audio = new Audio(audioFile);
  audio.autoplay = true;
  audio.play();
}



/************************************************************
** FORM
************************************************************/


/*
** Initialize the form
*/
function InitForm () {
  var mainContainer = GetMainContainer();

  // Create a container for the form
  var formContainer = document.createElement("div");
  formContainer.id = "formContainer";
  mainContainer.appendChild(formContainer);

  // Question
  var formMsg = document.createElement("span");
  formMsg.id = "formMsg";
  formMsg.classList.add("DefaultFontStyle", "MainMessage");
  formMsg.innerHTML = "How many fakes did you found ?";
  formContainer.appendChild(formMsg);

  // Input container
  var inputContainer = document.createElement("div");
  inputContainer.id = "inputContainer";
  inputContainer.classList.add("MainMessage");
  formContainer.appendChild(inputContainer);

  // Input number
  var formInput = document.createElement("input");
  formInput.id = "formInput";
  formInput.type = "number";
  formInput.placeholder = "Number of fake tweets";
  inputContainer.appendChild(formInput);

  // Valid button
  var formValid = document.createElement("button");
  formValid.id = "formValid";
  formValid.classList.add("DefaultFontStyle");
  formValid.innerHTML = "Confirm";
  formValid.addEventListener("click", CheckForm);
  inputContainer.appendChild(formValid);

  // If the user types "Enter" rather than pressing the button
  document.addEventListener("keypress", KeyPressed);
}

function KeyPressed (event) {
  if (event.key === "Enter") {
    if (document.getElementById("formInput"))
    {
      CheckForm();
    }
  }
}

/*
** Valid the answer
*/
function CheckForm () {
  var input = document.getElementById("formInput");
  var warning = document.getElementById("formWarning");

  // Check the input value
  if (input.value) {
    document.removeEventListener("keypress", KeyPressed);
    InitEndMsg(input.value);
    CleanFormScreen();
  } else {
    // Show warning message only once
    if (!document.getElementById("formWarning")) {
      CreateFormWarningMsg();
    }else{
      ShowFormWarningMsg();
    }
  }
}


/*
** Add a warning message to the form
*/
function CreateFormWarningMsg () {
  var formContainer = document.getElementById("formContainer");

  // Warning message
  var formWarning = document.createElement("span");
  formWarning.id = "formWarning";
  formWarning.classList.add("DefaultFontStyle", "MainMessage", "WarningFadeOut");
  formWarning.innerHTML = "You must enter the number of fake tweets you have found";
  formContainer.appendChild(formWarning);
}


/*
** Makes the message reappear
*/
function ShowFormWarningMsg () {
  var formWarning = document.getElementById("formWarning");
  var clone = formWarning.cloneNode(true);

  // Replace the element to trigger the css animation
  formWarning.parentNode.replaceChild(clone, formWarning);
}


/*
** Remove all element from the form
*/
function CleanFormScreen () {
  var mainContainer = GetMainContainer();
  var formContainer = document.getElementById("formContainer");

  mainContainer.removeChild(formContainer);
}



/************************************************************
** END MESSAGE
************************************************************/


/*
** Show end message depending on the number answered
*/
function InitEndMsg (answer) {
  var mainContainer = GetMainContainer();

  // Create an end container
  var endContainer = document.createElement("div");
  endContainer.id = "endContainer";
  mainContainer.appendChild(endContainer);

  // Contents of the messages
  var msgContent = "";
  var msgContent2 = "";

  if (answer == noFakeTweets) {
    // SUCCESS
    msgContent = "You're right ! ";
    msgContent2 = "Keep a critical mind and don't believe everything you see from Internet.";
  } else {
    // FAIL
    msgContent = "You're wrong. ";
    msgContent2 = "Try to be more critical and don't believe everything you see from Internet.";
  }

  if (noFakeTweets == 0)
  {
    msgContent += "There was no fake tweet in this sequence.";
  }
  if (noFakeTweets == 1)
  {
    msgContent += "There was only one fake tweet in this sequence.";
  }
  if (noFakeTweets > 1)
  {
    msgContent += "There were exactly " + noFakeTweets + " fake tweets in this sequence.";
  }

  // End message
  var endMsg = document.createElement("span");
  endMsg.id = "endMsg";
  endMsg.classList.add("DefaultFontStyle", "MainMessage");
  endMsg.innerHTML = msgContent;
  endContainer.appendChild(endMsg);

  // Second part of the message
  var endMsg2 = document.createElement("span");
  endMsg2.id = "endMsg2";
  endMsg2.classList.add("DefaultFontStyle", "MainMessage");
  endMsg2.innerHTML = msgContent2;
  endContainer.appendChild(endMsg2);

  // Restart button
  var endButton = document.createElement("button");
  endButton.id = "endButton";
  endButton.classList.add("DefaultFontStyle", "MainMessage");
  endButton.innerHTML = "End";
  endButton.addEventListener("click", Restart);
  endContainer.appendChild(endButton);
}


/*
** Clean the end screen and init the start screen
*/
function Restart () {
  CleanEndScreen();
  ChangeBackgroundColor("start");
  InitStart();
}


/*
** Remove all elements from the end message
*/
function CleanEndScreen () {
  var mainContainer = GetMainContainer();
  var endContainer = document.getElementById("endContainer");

  mainContainer.removeChild(endContainer);
}