const fs = require('fs');
const path = require('path');

var markov = require('../markov/index.js')

var Twitter = require('twitter');
var dataset = require('../data/dataset.json');
var fakes = require('../data/fakes.json');

var client = new Twitter({
    consumer_key: 'iuYccMWWHbWQzt5vnbsb2IskU',
    consumer_secret: '0HsmUaTvKpPmvMO280sRSG8Os9HbxaEtiwIuRcvbiLMeNjmwfD',
    access_token_key: '1234468882494185473-04FTfzHPVtJyKudiZ8wNxGCVwZW4ou',
    access_token_secret: 'kSpUCoDCWryPJIfxus4Qvun8Nx3Tr1iBzptr4sz8GEPkv'
});

async function getTweets(count) {    
    client.get('statuses/home_timeline', {count:count, tweet_mode:'extended'}, function(error, tweets, response) {
        if(error) throw error;
        var tweetList = {
            tweets: []
        }
        tweets.forEach(tweet => {
            tweetList.tweets.push(buildJsonTweet(tweet));
            dataset.tweets.push(buildDataTweet(tweet));
        });
        const data = JSON.stringify(tweetList, null, 4);
        fs.writeFileSync(path.resolve(__dirname, '../data/tweets.json'), data);
        const dataSet = JSON.stringify(dataset, null, 4);
        fs.writeFileSync(path.resolve(__dirname, '../data/dataset.json'), dataSet);

    });
}

function buildJsonTweet(tweet) {
    return {
        tweetId: tweet.id_str,
        account: tweet.user.screen_name
    }
}

function buildDataTweet(tweet) {
    var date_tweet = new Date(tweet.created_at);
    return {
        date: date_tweet.toISOString(),
        text: tweet.full_text
    }    
}

async function sendTweet() {
    let generated = await markov.generateTweet();
    client.post('statuses/update', {status: generated.string}, function(error, tweet, response) {
        if(error) {
            console.log(error);
        } else {
            console.log(tweet);
            fakes.tweets.push(buildJsonTweet(tweet));
            if(fakes.tweets.length >= 6) {
                fakes.tweets.shift();
            }
            const fakesStr = JSON.stringify(fakes, null, 4);
            fs.writeFileSync(path.resolve(__dirname, '../data/fakes.json'), fakesStr);
        }
    })
}

var minutes = 10, the_interval = minutes * 60 * 1000;
setInterval(function() {
    getTweets(25);
    sendTweet();
}, the_interval);