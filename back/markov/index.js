const Markov = require('markov-strings').default;
const fs = require('fs');
const os = require('os');

async function generateTweet() {
    var jsonFile = JSON.parse(fs.readFileSync(process.cwd() + '/back/data/dataset.json').toString());

    var tweets = jsonFile.tweets;
    
    var data = [];
    
    tweets.forEach(tweet => {
        var str = tweet.text;
        str = str.replace(os.EOL, ' ');
        if(str.endsWith(' ')) {
            str = str.substring(0, str.length-1);
        }
        if(!str.endsWith('.')) {
            str += '.';
        }
        if(str.startsWith(' ')) {
            str = str.substring(1, str.length);
        }
        if(str != os.EOL) {
            data.push(str);
        }
    });
    
    const markov = new Markov(data, { stateSize: 2 });
    markov.buildCorpus();
    
    const options = {
        maxTries: 10000,
        filter: (result) => {
              return result.string.split(' ').length >= 10 && result.string.split(' ').length <= 30 && result.score >= 1000
        }
    }
    
    const result = markov.generate(options);

    return result;
}

exports.generateTweet = generateTweet;

