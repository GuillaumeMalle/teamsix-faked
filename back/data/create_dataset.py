from twitter_scraper import *
import re
import json
from datetime import datetime, timedelta

l = open("list_account.txt", "r")

data = {}
data['tweets'] = []
past = datetime.now() - timedelta(days=5)

for acc in l.readlines():
    print("retrieving tweets from @" + acc)
    for tweet in get_tweets(acc[:-1], pages=100):
        if(tweet['time'] >= past):
            text = tweet['text']
            text = re.sub(r'https?:\/\/.*[\r\n]*', '', text)
            text = re.sub(r'pic.twitter.com\/.*$', '', text)
            data['tweets'].append({
                'date': tweet['time'].strftime("%m/%d/%Y, %H:%M:%S"),
                'text': text
            })

json_formated = json.dumps(data, indent=2)

with open('dataset.json', 'w') as outfile:
    outfile.write(json_formated)